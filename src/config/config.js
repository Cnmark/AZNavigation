// default config
module.exports = {
  port: 1234,

  // 可以公开访问的Controller
  publicController: [
    // 格式为controller
    'index',
    'auth',
    'goods',
    'brand',
    'search',
    'region'
  ],

  // 可以公开访问的Action
  publicAction: [
    // 格式为： controller+action
    'admin/login',
    'module_employ/post',
    'poi_navigation_employ/post',
    'poi_search_employ/post',
    'position_upload/post'
  ]
};
