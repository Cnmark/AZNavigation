module.exports = class extends think.Model {
  async listData(bid, fid, type, startDate, endDate) {
    let queryStr = '';
    let whereStr = '';

    if (bid == 0) {
      whereStr = 'where (building_name = 10133 or building_name = 10034)';
    } else {
      let temp = '';
      if (fid != 0) {
        temp = ' and floor_name = ' + fid;
      }
      whereStr = 'where (building_name = ' + bid + temp + ')';
    }
    if (type == 0) {
      queryStr = 'select DATE_FORMAT(use_time,\'%Y-%m-%d\') days,count(id) count from az_poi_navigation_employ ' +
        whereStr + ' and use_time> "' + startDate + ' 00:00:00" and use_time< "' + endDate +
        '  23:59:59" group by days;';
    } else {
      queryStr = 'select DATE_FORMAT(use_time,\'%Y-%m\') days,count(id) count from az_poi_navigation_employ ' +
        whereStr + ' and use_time> "' + startDate + ' 00:00:00" and use_time< "' + endDate +
        '  23:59:59" group by days;';
    }
    return this.query(queryStr);
  }

  async topData(bid, fid, type, startDate, endDate) {
    let queryStr = '';
    let whereStr = '';

    if (bid == 0) {
      whereStr = 'where (building_name = 10133 or building_name = 10034)';
    } else {
      let temp = '';
      if (fid != 0) {
        temp = ' and floor_name = ' + fid;
      }
      whereStr = 'where (building_name = ' + bid + temp + ')';
    }
    if (type == 0) {
      queryStr = 'select floor_name,poi_name,count(id) count from az_poi_navigation_employ ' +
        whereStr + ' and use_time> "' + startDate + ' 00:00:00" and use_time< "' + endDate +
        '  23:59:59" group by floor_name,poi_name  order BY  count desc LIMIT 0,10';
    } else {
      queryStr = 'select floor_name,poi_name,count(id) count from az_poi_navigation_employ ' +
        whereStr + ' and use_time> "' + startDate + ' 00:00:00" and use_time< "' + endDate +
        '  23:59:59" group by floor_name,poi_name  order BY  count desc LIMIT 0,10';
    }
    return this.query(queryStr);
  }
};
