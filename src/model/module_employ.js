module.exports = class extends think.Model {
  async listData(bid, type, startDate, endDate) {
    let queryStr = '';
    let whereStr = '';

    if (bid == 0) {
      whereStr = 'where (building_id = 10133 or building_id = 10034)';
    } else {
      whereStr = 'where (building_id = ' + bid + ')';
    }
    if (type == 0) {
      queryStr = 'select DATE_FORMAT(use_time,\'%Y-%m-%d\') days,count(id) count from az_module_employ ' +
        whereStr + ' and use_time> "' + startDate + ' 00:00:00" and use_time< "' + endDate +
        '  23:59:59" group by days;';
    } else {
      queryStr = 'select DATE_FORMAT(use_time,\'%Y-%m\') days,count(id) count from az_module_employ ' +
        whereStr + ' and use_time> "' + startDate + ' 00:00:00" and use_time< "' + endDate +
        '  23:59:59" group by days;';
    }
    return this.query(queryStr);
  }
};
