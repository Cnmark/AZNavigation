module.exports = class extends think.Service {
  getAllDates(begin, end, type) {
    let dateArray = [];
    const dateSet = new Set();
    const startTime = this.getDate(begin);
    const endTime = this.getDate(end);
    while ((endTime.getTime() - startTime.getTime()) >= 0) {
      const year = startTime.getFullYear();
      const month = startTime.getMonth().toString().length === 1 ? '0' + (startTime.getMonth() + 1).toString() : (startTime.getMonth() + 1);
      const day = startTime.getDate().toString().length === 1 ? '0' + startTime.getDate() : startTime.getDate();
      dateArray.push(year + '-' + month + '-' + day);
      dateSet.add(year + '-' + month);
      startTime.setDate(startTime.getDate() + 1);
    }
    if (type != 0) {
      dateArray = [];
      dateSet.forEach(function (e) {
        dateArray.push(e);
      });
    }
    return dateArray;
  }

  getDate(datestr) {
    const temp = datestr.split('-');
    const date = new Date(temp[0], temp[1] - 1, temp[2]);
    return date;
  }
};
