const Base = require('./base.js');
const moment = require('moment');
module.exports = class extends Base {
  async postAction() {
    const model = this.model('position_upload');
    const insertId = await model.add(
      {
        x: this.post('x'),
        y: this.post('y'),
        floor_name: this.post('floor_name'),
        building_id: this.post('building_id'),
        device_id: this.post('device_id'),
        create_time: moment().format('YYYY-MM-DD HH:mm:ss')
      });
    if (insertId > 0) {
      return this.success('', '上传数据成功');
    } else {
      return this.fail(100, '上传数据失败');
    }
  }
};
