module.exports = class extends think.Logic {
  postAction() {
    this.allowMethods = 'post';
    this.rules = {
      building_id: {
        length: {max: 20},
        string: true,
        trim: true,
        required: true
      },
      poi_name: {
        length: {max: 50},
        string: true,
        trim: true,
        required: true
      },
      floor_name: {
        int: true,
        trim: true,
        required: true
      },
      device_id: {
        length: {max: 50},
        string: true,
        trim: true
      }
    };
  }
};
