module.exports = class extends think.Logic {
  postAction() {
    this.allowMethods = 'post';
    this.rules = {
      x: {
        string: true,
        trim: true,
        required: true
      },
      y: {
        string: true,
        trim: true,
        required: true
      },
      building_id: {
        length: {max: 20},
        string: true,
        trim: true,
        required: true
      },
      floor_name: {
        int: true,
        trim: true,
        required: true
      },
      device_id: {
        length: {max: 50},
        string: true,
        trim: true
      }
    };
  }
};
