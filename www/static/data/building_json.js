const bjson =
  [
    {
      'building_id': 10034,
      'floor_info': [
        {'id': 1, 'name': 'F1'},
        {'id': 2, 'name': 'F2'},
        {'id': 3, 'name': 'F3'},
        {'id': 4, 'name': 'F4'},
        {'id': 5, 'name': 'F5'}
      ]
    },
    {
      'building_id': 10133,
      'floor_info': [
        {'id': 1, 'name': 'L1'},
        {'id': 2, 'name': 'L2'},
        {'id': 3, 'name': 'L3'},
        {'id': 4, 'name': 'L4'},
        {'id': 5, 'name': 'L5'},
        {'id': 6, 'name': 'L6'},
        {'id': 7, 'name': 'L7'}
      ]
    }
  ];
