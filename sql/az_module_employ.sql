/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : aslk

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2017-10-21 08:11:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for az_module_employ
-- ----------------------------
DROP TABLE IF EXISTS `az_module_employ`;
CREATE TABLE `az_module_employ` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `use_time` datetime NOT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `building_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of az_module_employ
-- ----------------------------


-- ----------------------------
-- Table structure for az_poi_navigation_employ
-- ----------------------------
DROP TABLE IF EXISTS `az_poi_navigation_employ`;
CREATE TABLE `az_poi_navigation_employ` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `floor_name` varchar(11) NOT NULL,
  `poi_name` varchar(255) NOT NULL,
  `use_time` datetime NOT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `building_name` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of az_poi_navigation_employ
-- ----------------------------


-- ----------------------------
-- Table structure for az_poi_search_employ
-- ----------------------------
DROP TABLE IF EXISTS `az_poi_search_employ`;
CREATE TABLE `az_poi_search_employ` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `floor_name` varchar(11) NOT NULL,
  `poi_name` varchar(255) NOT NULL,
  `use_time` datetime NOT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `building_name` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of az_poi_search_employ
-- ----------------------------

-- ----------------------------
-- Table structure for az_position_upload
-- ----------------------------
DROP TABLE IF EXISTS `az_position_upload`;
CREATE TABLE `az_position_upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `floor_name` varchar(255) NOT NULL,
  `building_id` varchar(255) NOT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of az_position_upload
-- ----------------------------


-- ----------------------------
-- Table structure for az_position_upload_his
-- ----------------------------
DROP TABLE IF EXISTS `az_position_upload_his`;
CREATE TABLE `az_position_upload_his` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `floor_name` varchar(255) NOT NULL,
  `building_id` varchar(255) NOT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of az_position_upload_his
-- ----------------------------

-- ----------------------------
-- Table structure for az_user
-- ----------------------------
DROP TABLE IF EXISTS `az_user`;
CREATE TABLE `az_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `last_login_time` int(11) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of az_user
-- ----------------------------
INSERT INTO `az_user` VALUES ('1', 'admin', '0d1453f007e1c845ea8c61ffba5696a5', '1', '1505813578', '::ffff:127.0.0.1');

-- ----------------------------
-- Table structure for t_positionrecord4xy
-- ----------------------------
DROP TABLE IF EXISTS `t_positionrecord4xy`;
CREATE TABLE `t_positionrecord4xy` (
  `fromtimestamp` varchar(20) DEFAULT NULL,
  `totimestamp` varchar(20) DEFAULT NULL,
  `buildingid` int(11) DEFAULT NULL,
  `floorno` varchar(20) DEFAULT NULL,
  `xaxle` double DEFAULT NULL,
  `yaxle` double DEFAULT NULL,
  `pointpeoplenumber` int(11) DEFAULT NULL,
  KEY `idx_xy_1` (`fromtimestamp`,`totimestamp`,`buildingid`,`floorno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
/*!50100 PARTITION BY KEY (BuildingID)
PARTITIONS 5 */;

-- ----------------------------
-- Records of t_positionrecord4xy
-- ----------------------------

